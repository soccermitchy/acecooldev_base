//
// Simple Helper Functions to decrease code - Josh 'Acecool' Moser
//


// Example Usage:
/*
It's hooked up to the same standard, that you can take the first 5 args and copy/paste them from the top layer, from the previous 2
that were needed, simply copy and base, and it's done.

Example:
This:
draw.RoundedBorderedBox( 8, ScrW( ) / 2 - ( b + _textW ) / 2, -b, b + _textW, b + 25, Color( 50, 50, 50, 200 ), 1, Color( 250, 250, 250, 200 ) )

Is the same as:
draw.RoundedBox( 8, ScrW( ) / 2 - ( b + _textW ) / 2 - 1, -b + 1, b + _textW + 2, h or b + 25, Color( 250, 250, 250, 200 ) )
draw.RoundedBox( 8, ScrW( ) / 2 - ( b + _textW ) / 2, -b, b + _textW, h or b + 25, Color( 50, 50, 50, 200 ) )

By copying this straight into the new function, adding the dominant color, the border size and the border color: 
8, ScrW( ) / 2 - ( b + _textW ) / 2, -b, b + _textW, h or b + 25



This:
draw.RoundedBorderedBox( 8, -b, -b, b*3 + _textW + 15, h or b + 25, Color( 50, 50, 50, 200 ), 1, Color( 250, 250, 250, 200 ) )

Is the same as:
draw.RoundedBox( 8, -b, -b + 1, b*3 + _textW + 15 + 1, h or b + 25, Color( 250, 250, 250, 200 ) )
draw.RoundedBox( 8, -b, -b, b*3 +_textW + 15, h or b + 25, Color( 50, 50, 50, 200 ) )

By copying this straight into the new function, adding the dominant color, the border size and the border color:
8, -b, -b, b*3 +_textW + 15, h or b + 25



This:
draw.RoundedBorderedBox( 8, ScrW( ) - w, -b, ScrW( ) - ( ScrW( ) - w - b ), h or b + 25, Color( 50, 50, 50, 200 ), 1, Color( 250, 250, 250, 200 ) )

Is the same as:
draw.RoundedBox( 8, ScrW( ) - w - 1, -b + 1, ScrW( ) - ( ScrW( ) - w - b ), h or b + 25, Color( 250, 250, 250, 200 ) )
draw.RoundedBox( 8, ScrW( ) - w, -b, ScrW( ) - ( ScrW( ) - w - b ), h or b + 25, Color( 50, 50, 50, 200 ) )

By copying this straight into the new function, adding the dominant color, the border size and the border color:
8, ScrW( ) - w, -b, ScrW( ) - ( ScrW( ) - w - b ), h or b + 25
*/



// Example Hook
hook.Add( "HUDPaint", "HUDTest", function( )
	draw.RoundedBorderedBox( 2, 50, 50, 100, 25, COLOR_BLACK, 1, COLOR_WHITE )
	draw.RoundedBox( 0, 50, 150, 100, 25, COLOR_WHITE )
end );

//
// draw.RoundedBorderedBoxEx is identical, except it allows you to set each individual corner as rounded or flat...
//