README
What is this repository for?

This repo is of a base / skeleton game-mode with many helper functions, metatable objects / classes, algorithms, and systems to help speed up the game-mode creation process. Much of the code is written in a way that it can be used in an easy way. Examples: If you use Material( ) inside of HUDPaint, you'll overflow / run out of memory; you can, however, use draw.GetMaterial in HUDPaint - it'll manage the materials for you. Also if you use ClientsideModel in a render-hook, you'll overflow / run out of memory; you can, however, use render.ClientsideModel without the issue. Same goes for ParticleEmitter vs LocalPlayer( ):GetEmitter( ); etc...
How do I get set up?

To create an SRCDS: https://dl.dropboxusercontent.com/u/26074909/tutoring/server_srcds_steamcmd/setting_up_a_server_with_steamcmd.lua.html Add this repo to the server/garrysmod/gamemodes/ folder When you launch the game-mode, make sure the +gamemode is acecooldev_base If you use Windows, Autorefresh will be enabled; if you use Linux, you must type loadgm in the realm you want to update such as server-console for server/shared files, and GarrysMod game-window console for client/shared
Contribution guidelines

Please follow this code-standard: https://dl.dropboxusercontent.com/u/26074909/tutoring/_tutorial_quizzes/_coding_standards.lua.html
