/*******************************************************************************************
 *
 * 						Acecool Company: AcecoolDev Base for Garry's Mod
 * 				Josh 'Acecool' Moser :: CEO & Sole Proprietor of Acecool Company
 * 			 Skeleton++ Game-Mode with many useful helper-functions and algorithms
 *
 * 			 Copyright � 2001-2014 Acecool Company in Name & Josh 'Acecool' Moser
 *     as Intellectual Property Rights Owner & Sole Proprietor of Acecool Company in name.
 *
 * 					 RELEASED Under the ACL ( Acecool Company License )
 *
 *******************************************************************************************/

//
// Game-Mode Information
//
GM.Name 			= "AcecoolBaseGamemode";
GM.Author 			= "Base by Acecool; edits by ___________";
GM.Email 			= "acecoolcompany@gmail.com";
GM.Website 			= "http://www..com/";
GM.TeamBased 		= false;


//
// Include files below. I like keeping it in shared just so code isn't repeated.
// 	For individual realms, wrap in if SERVER then else end for server, or client.
// 	I typically don't repeat code; but there are a few locations here where code is repeated.
//


//
// Init
//
loader = { };


//
// Enumerations
//
local REALM_CLIENT 	= 0;
local REALM_SERVER 	= 1;
local REALM_SHARED 	= 2;


//
// Vars
//
local REALMS = {
	[ REALM_CLIENT ] = "Client";
	[ REALM_SHARED ] = "Shared";
	[ REALM_SERVER ] = "Server";
};


//
// Helper-Function to reduce the amount of code repetition for including files..
// ( GM or GAMEMODE ).FolderName .. "/"
function loader:IncludeFiles( _gmfolder, _folder, _file, _realm ) -- 9 lines
	// If no realm specified, error out...
	if ( !_realm ) then Error( "Realm unset, can't include: " .. _folder .. _file ); end

	// The only time AddCSLuaFile is called, is by the SERVER. Keeping that in mind,
	// 	when would we need the client to download the file? If it is REALM_SHARED or CLIENT file.
	if ( SERVER && ( _realm == REALM_CLIENT || _realm == REALM_SHARED ) ) then
		if ( !GAMEMODE_FILES[ REALM_SERVER ].csfiles[ _folder ] ) then GAMEMODE_FILES[ REALM_SERVER ].csfiles[ _folder ] = { }; end
		table.insert( GAMEMODE_FILES[ REALM_SERVER ].csfiles[ _folder ], _file );
		AddCSLuaFile( _gmfolder .. "/" .. _folder .. _file );
	end

	// We call include many different ways: If the realm is REALM_SERVER and the SERVER executed the code,
	// 	or if the realm is REALM_CLIENT and the CLIENT executed the code, or if the realm is REALM_SHARED meaning
	// 	we don't need to check if SERVER or CLIENT as BOTH should include at that point.
	if ( ( SERVER && _realm == REALM_SERVER ) || ( CLIENT && _realm == REALM_CLIENT ) || ( ( CLIENT || SERVER ) && _realm == REALM_SHARED ) ) then
		if ( !GAMEMODE_FILES[ _realm ].files[ _folder ] ) then GAMEMODE_FILES[ _realm ].files[ _folder ] = { }; end
		table.insert( GAMEMODE_FILES[ _realm ].files[ _folder ], _file );
		GAMEMODE_FILES[ _realm ].count = GAMEMODE_FILES[ _realm ].count + 1;
		table.insert( GAMEMODE_FILES.load_order, "[" .. string.upper( REALMS[ _realm ] ) .. "] :: " .. _folder .. _file );

		include( _gmfolder .. "/" .. _folder .. _file );
	end
end


//
// Prefix system - instead of parsing the string many times over for each 2, 3, 4 char prefix and 2, 3 char postfixes
// just generate them once and reference the values when needed.
//
local FILE_PRE_POST_FIX = nil;
function loader:GeneratePreAndPostFixes( _file )
	FILE_PRE_POST_FIX = {
		pre = {
			"";
			string.sub( _file, 1, 2 ); // XXre_filename.lua
			string.sub( _file, 1, 3 ); // XXXe_filename.lua
			string.sub( _file, 1, 4 ); // XXXX_filename.lua
		};
		post = {
			"";
			string.sub( _file, -6, -5 ); // _pre_filenaXX.lua
			string.sub( _file, -7, -5 ); // _pre_filenXXX.lua
		};
	};

	return FILE_PRE_POST_FIX;
end


//
// Checks to see if a file has pre/postfix
//
function loader:IsPrefix( _file, _chars, _string )
	if ( !FILE_PRE_POST_FIX ) then GeneratePreAndPostFixes( _file ); ErrorNoHalt( "GeneratePreAndPostFixes wasn't called prior to using IsPrefix... - It has been called for you!\n" ); end
	return ( _chars > 0 ) && FILE_PRE_POST_FIX.pre[ _chars ] == _string || FILE_PRE_POST_FIX.post[ math.abs( _chars ) ] == _string;
end


//
// Simple Recursion to include all files in each directory with proper inclusion logic
// Using a callback for demonstration purposes and to simplify code.
//
function loader:ProcessDirectoryFiles( _folder, _file_types, _realm, _callback, _count )
	// Grab the list of files and folders in the current directory.
	local _files, _folders = file.Find( ( GM or GAMEMODE ).FolderName .. "/" .. _folder .. "*", "LUA" );
	if ( table.HasValue( _folders, "NOLOAD" ) ) then return _count; end

	// Recurse through directories, this ensures deepest runs first,
	// alternatively move this below the file processor to have shallow run first.
	for k, v in pairs( _folders ) do
		// MAPS Directory: Load map entity additions, map configs, etc at InitPostEntity, and Add an OnReloaded for AutoRefresh compatibility
		if ( string.lower( v ) == "maps" ) then
			hook.Add( "InitPostEntity", "LoadLater", function( )
				self:ProcessDirectoryFiles( _folder .. v .. "/" .. string.lower( game.GetMap( ) ) .. "/", _file_types, REALM_SHARED, _callback );
			end );

			hook.Add( "OnReloaded", "LoadLater", function( )
				self:ProcessDirectoryFiles( _folder .. v .. "/" .. string.lower( game.GetMap( ) ) .. "/", _file_types, REALM_SHARED, _callback );
			end );
		else
			// Update the realm for the next pass-through
			local _newrealm = _realm;
			if ( string.lower( v ) == "client" ) then _newrealm = REALM_CLIENT; end
			if ( string.lower( v ) == "server" ) then _newrealm = REALM_SERVER; end
			if ( string.lower( v ) == "shared" ) then _newrealm = REALM_SHARED; end

			// Process folders
			self:ProcessDirectoryFiles( _folder .. v .. "/", _file_types, _newrealm, _callback );
		end
	end

	// Process files - using callback for sake of helping to understand Lua
	for k, v in pairs( _files ) do
		// Generate the prefix/postfix table for this file. Only 1 thread, so I can do this, I don't need
		// to return but I did anyway in case threading ever getsimplemented.
		local _tab = self:GeneratePreAndPostFixes( v )

		// x files skip loading, non lua files skip loading.
		if ( self:IsPrefix( v, 2, "x_" ) || self:IsPrefix( v, 3, "_x_" ) || self:IsPrefix( v, -2, "_x" ) ) then continue; end //  || !string.GetExtensionFromFilename( v ) != "lua"

		// Check for cl_, _cl_, _cl.lua, sh_, _sh_, _sh.lua, sv_, _sv_, _sv.lua for specific loading realm...
		local _bFileStartsCL = ( self:IsPrefix( v, 3, "cl_" ) || self:IsPrefix( v, 4, "_cl_" ) || self:IsPrefix( v, -3, "_cl" ) );
		local _bFileStartsSH = ( self:IsPrefix( v, 3, "sh_" ) || self:IsPrefix( v, 4, "_sh_" ) || self:IsPrefix( v, -3, "_sh" ) );
		local _bFileStartsSV = ( self:IsPrefix( v, 3, "sv_" ) || self:IsPrefix( v, 4, "_sv_" ) || self:IsPrefix( v, -3, "_sv" ) );

		// Update the realm for the include
		local _newrealm = _realm;
		_newrealm = ( _bFileStartsCL ) && REALM_CLIENT || _newrealm;
		_newrealm = ( _bFileStartsSH ) && REALM_SHARED || _newrealm;
		_newrealm = ( _bFileStartsSV ) && REALM_SERVER || _newrealm;

		// Call our helper-function to include the file
		self:IncludeFiles( ( GM or GAMEMODE ).FolderName, _folder, v, _newrealm )

		// If the callback is set, call the callback function referenced by _callback variable...
		if ( _callback ) then
			_callback( ( GM or GAMEMODE ).FolderName, _folder, v, _newrealm );
		end

		// Reset the prefix system:
		FILE_PRE_POST_FIX = nil;
	end
end


//
// LoadGameMode function
//
MAP_SCALE = 1;
if ( !GAMEMODE_LOADED ) then GAMEMODE_LOADED = false; end
function loader:LoadGameMode( )
	// Init / Reset Vars
	GAMEMODE_FILES = {
		load_order = { };
		[ REALM_CLIENT ] = {
			count = 0;
			files = { };
		};
		[ REALM_SHARED ] = {
			count = 0;
			files = { };
		};
		[ REALM_SERVER ] = {
			count = 0;
			files = { };
			csfiles = { };
		};
	};

	// Process game-mode files... Always load shared first, then it won't matter client/server next, then addons, then content...
	// It is done this way so enums, functions, etc have time to initialize before calling for addons and content...
	// Content dir is recursed too, but any maps dirs are loaded after InitPostEntity so if you want to make entities spawn in the map
	// like permanent props, you'll want to do that logic in a maps folder...
	self:ProcessDirectoryFiles( "gamemode/shared/", "*.lua", REALM_SHARED, function( _gmfolder, _folder, _file, _realm ) end );
	self:ProcessDirectoryFiles( "gamemode/client/", "*.lua", REALM_CLIENT, function( _gmfolder, _folder, _file, _realm ) end );
	self:ProcessDirectoryFiles( "gamemode/server/", "*.lua", REALM_SERVER, function( _gmfolder, _folder, _file, _realm ) end );
	self:ProcessDirectoryFiles( "gamemode/addons/", "*.lua", REALM_SHARED, function( _gmfolder, _folder, _file, _realm ) end );
	self:ProcessDirectoryFiles( "gamemode/content/", "*.lua", REALM_SHARED, function( _gmfolder, _folder, _file, _realm ) end );

	// If the fileio class is present, and toprint exists, then write the data to a file for debugging purposes
	if ( fileio && toprint ) then
		local _loOutput = fileio:New( );
		_loOutput:Write( "gm_files", "files_sh", toprint( GAMEMODE_FILES[ REALM_SHARED ] ) );
		_loOutput:Write( "gm_files", "files_cl", toprint( GAMEMODE_FILES[ REALM_CLIENT ] ) );
		_loOutput:Write( "gm_files", "files_sv", toprint( GAMEMODE_FILES[ REALM_SERVER ].count ) .. "\n" .. toprint( GAMEMODE_FILES[ REALM_SERVER ].files ) );
		_loOutput:Write( "gm_files", "files_addcs", toprint( GAMEMODE_FILES[ REALM_SERVER ].csfiles ) );
		_loOutput:Write( "gm_files", "files_order", toprint( GAMEMODE_FILES.load_order ) );
	end

	//
	GAMEMODE_LOADED = true;

	return true;
end


//
// Derive Base Game-Mode
//
if ( !GAMEMODE_LOADED ) then
	MsgC( Color( 255, 0, 0, 255 ), "Deriving BASE!" );
	DeriveGamemode( "base" );
end


//
// Load Game-Mode
//
loader:LoadGameMode( );


//
// Show loaded file-count
//
MsgC( COLOR_GREEN, string.rep( "*", 50 ) .. "\n" );
MsgC( COLOR_WHITE, "GameMode loaded " .. ( GAMEMODE_FILES[ REALM_CLIENT ].count + GAMEMODE_FILES[ REALM_SHARED ].count + GAMEMODE_FILES[ REALM_SERVER ].count ) .. " files!\n" )
MsgC( COLOR_GREEN, string.rep( "*", 50 ) .. "\n" );


//
// Fake Auto-Refresh to get by the annoyance of saving multiple files for new files to show up...
//
if ( SERVER ) then
	concommand.Add( "loadgm", function( _p, _cmd, _args )
		if ( _p != NULL && !_p:IsOwner( ) ) then return; end
		GM = GAMEMODE;
		loader:LoadGameMode( );
		BroadcastLua( "GM = GAMEMODE; loader:LoadGameMode( );" );
	end );

	local _folder_structure = {
		//
		// AcecoolDev_Base Folder Structure
		//
		"acecooldev_base/";
		"acecooldev_base/content/";
		"acecooldev_base/documentation/";
		"acecooldev_base/entities/";
		"acecooldev_base/gamemode/";

		//
		// Content Folder Structure
		//
		"acecooldev_base/content/materials/";
		"acecooldev_base/content/models/";
		"acecooldev_base/content/scripts/";
		"acecooldev_base/content/scripts/vehicles/";
		"acecooldev_base/content/sound/";

		//
		// Entities Folder Structure
		//
		"acecooldev_base/entities/effects/";
		"acecooldev_base/entities/entities/";
		"acecooldev_base/entities/weapons/";

		//
		// GameMode Folder Structure
		//
		"acecooldev_base/gamemode/addons/";
		"acecooldev_base/gamemode/client/";
		"acecooldev_base/gamemode/content/";
		"acecooldev_base/gamemode/server/";
		"acecooldev_base/gamemode/shared/";

		//
		// Client Folder Structure
		//
		"acecooldev_base/gamemode/client/_definitions/";
		"acecooldev_base/gamemode/client/classes/";
		"acecooldev_base/gamemode/client/core/";
		"acecooldev_base/gamemode/client/entity/";
		"acecooldev_base/gamemode/client/gui/";
		"acecooldev_base/gamemode/client/hooks/";
		"acecooldev_base/gamemode/client/player/";
		"acecooldev_base/gamemode/client/vehicle/";

		//
		// Content Folder Structure
		//
		"acecooldev_base/gamemode/content/events/";
		"acecooldev_base/gamemode/content/items/";
		"acecooldev_base/gamemode/content/jobs/";
		"acecooldev_base/gamemode/content/maps/";
		"acecooldev_base/gamemode/content/npcs/";
		"acecooldev_base/gamemode/content/player/";
		"acecooldev_base/gamemode/content/vehicle_attachments/";
		"acecooldev_base/gamemode/content/vehicles/";

		//
		// Server Folder Structure
		//
		"acecooldev_base/gamemode/server/_definitions/";
		"acecooldev_base/gamemode/server/core/";
		"acecooldev_base/gamemode/server/entity/";
		"acecooldev_base/gamemode/server/hooks/";
		"acecooldev_base/gamemode/server/player/";
		"acecooldev_base/gamemode/server/vehicle/";

		//
		// Shared Folder Structure
		//
		"acecooldev_base/gamemode/shared/_definitions/";
		"acecooldev_base/gamemode/shared/classes/";
		"acecooldev_base/gamemode/shared/core/";
		"acecooldev_base/gamemode/shared/entity/";
		"acecooldev_base/gamemode/shared/hooks/";
		"acecooldev_base/gamemode/shared/languages/";
		"acecooldev_base/gamemode/shared/net/";
		"acecooldev_base/gamemode/shared/player/";
		"acecooldev_base/gamemode/shared/vehicle/";
		"acecooldev_base/gamemode/shared/weapon/";

	};
	concommand.Add( "install", function( _p, _cmd, _args )
		if ( _p != NULL && !_p:IsSuperAdmin( ) ) then return; end
		for k, v in pairs( _folder_structure ) do
			if ( !file.Exists( v, "DATA" ) ) then
				print( "Creating DIR: ", v );
				file.CreateDir( v );
			end
		end
		file.Write( "acecooldev_base/readme.txt", "Merge this folder-structure into the downloaded Skeleton++ GameMode!", "DATA" )
		MsgC( Color( 0, 255, 255, 255 ), string.rep( "*", 50 ) );
		MsgC( Color( 255, 0, 255, 255 ), "\nFolder Structure set up. Merge garrysmod/data/acecooldev_base/* folder structure with gamemodes/acecooldev_base/\n" );
		MsgC( Color( 0, 255, 255, 255 ), string.rep( "*", 50 ) .. "\n" );
	end );
end