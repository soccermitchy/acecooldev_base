/*******************************************************************************************
 *
 * 						Acecool Company: AcecoolDev Base for Garry's Mod
 * 				Josh 'Acecool' Moser :: CEO & Sole Proprietor of Acecool Company
 * 			  Skeleton Game-Mode with many useful helper-functions and algorithms
 *
 * 			 Copyright � 2001-2014 Acecool Company in Name & Josh 'Acecool' Moser
 *     as Intellectual Property Rights Owner & Sole Proprietor of Acecool Company in name.
 *
 * 					 RELEASED Under the ACL ( Acecool Company License )
 *
 *******************************************************************************************/

//
//
//
AddCSLuaFile( "sh_init.lua" )
AddCSLuaFile( "cl_init.lua" )
include( "sh_init.lua" );